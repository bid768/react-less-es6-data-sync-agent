import React from 'react'
import { Route, HashRouter, NavLink } from 'react-router-dom'
import Home from 'modules/home/home.page'
import Detail from 'modules/detail/detail'

const MyMenu = () => {
    return (
        <div>
            <NavLink to="/">首页</NavLink>
            <NavLink to="/detail">详情</NavLink>
        </div>
    )
}

class App extends React.Component {
    render() {
        return (
            <HashRouter>
                <MyMenu />
                <div style={{ width: '100%', height: '100%' }}>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/detail" component={Detail} />
                </div>
            </HashRouter>
        )
    }
}

export default App
