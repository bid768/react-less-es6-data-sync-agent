import React from 'react'
import ReactDom from 'react-dom'
import axios from 'axios'
import Evt from 'js/event/eventsEmitter.js'
import WebSocketClient from 'js/network/webSocketClient.js'
import App from './app'

const render = () => {
    global.ws = new WebSocketClient(global.config.sdsURL, global.config.sdsRoom)
    global.ws.connect()
    global.ws.onMessage = data => {
        const dataObj = JSON.parse(data)
        console.log('websocket event：', dataObj)
        Evt.emit(dataObj.event, dataObj)
    }
    ReactDom.render(<App />, document.getElementById('root'))
}

axios.get('./config/config.json').then(res => {
    global.config = res.data
    render()
})
