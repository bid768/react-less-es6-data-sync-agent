import React from 'react'
import BaseComponent from 'components/baseComponent'
import anime from 'animejs'

class Anime extends BaseComponent {
    constructor(props) {
        super(props)
        this.animeTime = 0
        this.anime = null
        this.state = {
            animeTime: 0,
            sendTime: 0
        }
    }

    render() {
        return (
            <div id="animeTest" style={{ width: '256px', height: '256px' }}>
                <img src="/assets/img/timg.jpg" />
            </div>
        )
    }

    componentDidMount() {
        this.anime = anime({
            targets: '#animeTest',
            rotate: 360,
            loop: true,
            autoplay: this.isMaster,
            duration: 3000,
            easing: 'linear',
            update: anime => {
                this.animeTime = anime.currentTime
            }
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (!this.isMaster) {
            if (prevState.sendTime !== this.state.animeTime) {
                const seekTime =
                    new Date().getTime() -
                    this.state.sendTime +
                    this.state.animeTime
                this.anime.seek(seekTime)
                this.anime.play()
            }
        }
    }

    joinPage() {
        if (this.isMaster) {
            this.anime.play()
            this.setStateSync({
                animeTime: this.animeTime,
                sendTime: new Date().getTime()
            })
        }
    }
}

export default Anime
