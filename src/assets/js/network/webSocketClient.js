class WebSocketClient {
    constructor(url, room = 'room') {
        //消息队列
        this.msgQueue = []
        this.connected = false
        this.url = url
        this.socket = null
        //是否手动关闭，手动关闭不自动重连
        this.accClose = false
        this.room = room

        this.onMessage = null
        this.onOpen = null
        this.onClose = null

        //消息队列的最大长度
        this.maxMess = 500

        this.loopMsgQueue()
    }

    //连接
    connect(onOpen) {
        if (this.connected) {
            return
        }
        if (onOpen) {
            this.onOpen = onOpen
        }
        this.accClose = false
        this.socket = new WebSocket(this.url)
        this.socket.onopen = event => {
            this.connected = true
            if (this.onOpen) {
                this.onOpen()
            }
            console.log(this.url + '连接成功')
        }

        this.socket.onclose = event => {
            this.connected = false
            if (this.onClose) {
                this.onClose()
            }
            console.log(this.url + '连接关闭')
            if (!this.accClose) {
                this.connect()
            }
        }

        this.socket.onerror = event => {
            this.connected = false
            console.log(this.url + '连接错误')
        }

        this.socket.onmessage = event => {
            if (this.onMessage) {
                this.onMessage(event.data)
            }
        }
    }

    //发送数据
    emit(event, data = {}, sendNow = false) {
        data.event = event
        if (event == 'join') {
            data.room = this.room
        }
        data = JSON.stringify(data)
        if (this.msgQueue.length > this.maxMess) {
            this.msgQueue.slice(0, 1)
        }
        if (sendNow) {
            this.msgQueue.unshift(data)
        } else {
            this.msgQueue.push(data)
        }
    }

    //关闭
    close(msg) {
        this.accClose = true
        this.socket.close(1000, msg)
    }

    //消息队列循环
    loopMsgQueue() {
        window.cancelAnimationFrame(this.loopId)
        if (this.connected && this.msgQueue.length > 0) {
            let item = this.msgQueue.shift()
            this.socket.send(item)
        }
        this.loopId = window.requestAnimationFrame(this.loopMsgQueue.bind(this))
    }
}
export default WebSocketClient
