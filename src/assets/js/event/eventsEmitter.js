const EventEmitter = require('events').EventEmitter
const event = new EventEmitter()
event.setMaxListeners(100)

export default event
