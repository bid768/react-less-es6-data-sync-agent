# 数据同步客户端

websocket通信，需要先启动服务端，再启动客户端，就能看到如下效果。


![效果图](./doc/result.gif)


## 三、运行

1、安装依赖

````
yarn install
````

2、启动客户端

````
npm run dev
````
